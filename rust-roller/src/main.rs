use rand::{thread_rng, Rng};
use std::env;

fn main() {
    let args: Vec<String> = env::args().collect();
    let mut rng = thread_rng();

    let mut sum: u32 = 0;

    let dice: u32 = args[1].parse::<u32>().unwrap();
    let sides: u32 = args[2].parse::<u32>().unwrap();

    print!("Dice: ");
    let mut loop_num = 0;
    loop {
        if loop_num == dice { break; }
        let cur_die: u32 = rng.gen_range(1..=sides);
        print!("{} ", cur_die);

        sum = sum + cur_die;
        loop_num = loop_num + 1;
    }
    print!("\n");
    
    println!("Total: {}", sum);
}